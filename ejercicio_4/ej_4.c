#include <stdio.h>

int main(){
	int *pnumero;
	int num1, num2;
	char *pchar;
	char letra1;
	num1 = 2;
	num2 = 5;
	letra1 = 'a';

	pnumero = &num1;
	printf("\nLa direccion de memoria a la que apunte el puntero PNUMERO es: %p\n\n", pnumero);
	printf("El valor que contiene la direccion de memoria que guarda PNUMERO es: %i\n\n",*pnumero);
	}
