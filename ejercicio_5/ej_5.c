#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void cargarAleatorio( int *vector);

int main(){
	int i,k,aux;
	int *vector;
	vector = (int*)malloc(10*sizeof(int));
	if(vector == NULL){
		printf("Error al asignar la memoria dinamica");
	}else{
		cargarAleatorio(vector);

		//Ordenando el vector
		for(i = 0; i < 10; i++){
			for(k = 0; k < 10 - i; k++){
				if(*(vector + k) <  *(vector + k + 1)){
					aux = *(vector + k);
					*(vector + k) = *(vector + k + 1);
					*(vector + k + 1) = aux;
				}
			}
		}
		printf("Numeros ordenados\n");
		for(i = 0; i < 10; i++){
			printf("\nNumero:   %i", *(vector+i));
		}
		printf("\n");
	}
	return 0;
}

void cargarAleatorio(int *vector){

	int x;
	srand(time(NULL));
	for(x = 0; x < 10; x++){
		* (vector + x) = rand() % 100;
		printf("valor ramdom insertado:   %i\n", *(vector + x));
	}
	printf("\n");
}
