#include <stdio.h>

int main(){
	//Declaracion de variables
	float n1;
	float n2;
	float *p1;
	float *p2;
	//fin

	//Asignacion de valor a las variables
	n1 = 4.0;
	p1 = &n1;
	n2 = *p2;
	n1 = *p1 + *p2;
	//fin

	//Muestra en pantalla de valores en variables
	printf("\nValor de n1: %.2f\n", n1);
	printf("Valor de n2: %.2f\n", n2);
	//fin

	return(0);
}
