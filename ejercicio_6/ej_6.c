#include <stdio.h>
#include <stdlib.h>

int main(){
	int i;
	int x,y,aux;
	float *vector;
	float total=0.0;

	vector  = (float*)malloc(5*sizeof(float));
	if(vector == NULL){
		printf("Asignacion de memoria dinamica fallida");
	}else{
		for(i = 0; i < 5; i++){
			printf("\n Digite un numero: ");
			scanf("%f",(vector+i));
		}

		for(x = 0; x < 5; x++){
			total = total + *(vector+x); 
			for(y = 0; y < 5 - x; y++){
				if( *(vector + y) < *(vector + y + 1)){
					aux = *(vector + y);
					*(vector + y) = *(vector + y +1);
					*(vector + y + 1) = aux;
				}
			}
		}

		printf("\nNumero mayor es: %.2f\n", *vector);
		printf("Numero menor es: %.2f\n", *(vector + 4));
		printf("Media aritmetica es: %.2f\n", (total/5));
	}
	return 0;
}
